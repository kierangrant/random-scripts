#!/bin/bash
LO_BASE=/home/kieran/.cache/find-in-docs
find \( -name "*.doc*" -o -name "*.odt" -o -name "*.xls*" -o -name "*.ods" \) -exec sh -c "libreoffice24.8 -env:UserInstallation=file://$LO_BASE --cat \"{}\" 2>/dev/null | grep --label=\"{}\" -H -E -i -- \"$1\"" \;
