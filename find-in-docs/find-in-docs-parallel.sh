#!/bin/bash
BASEPATH=/usr/local/libexec/find-in-docs
LO_BASE=$HOME/.cache/find-in-docs
if [ ! -d $LO_BASE ] # base directory doesn't exist
then
    echo "\"$LO_BASE\" does not exist, creating..."
    libreoffice24.2 -env:UserInstallation=file://$LO_BASE --version 2>/dev/null
fi
find \( -name "*.doc*" -o -name "*.odt" -o -name "*.xls*" -o -name "*.ods" \) -exec  parallel -j $((`nproc`*3/4)) "$BASEPATH/find-in-docs-parallel-worker.sh" "$1" -- {} +
