#!/bin/bash
LO_BASE=$HOME/.cache/find-in-docs
LO_SOURCE=$LO_BASE/source
LO_TMP=$LO_BASE/$$.tmp
LO_MOUNT=$LO_BASE/$$

trap cleanup 1 2 3 6

cleanup()
{
  echo "Caught Signal ... cleaning up."
  umount $LO_MOUNT
  rmdir $LO_MOUNT
  rm -rf $LO_TMP
  echo "Done cleanup ... quitting."
  exit 1
}

mkdir $LO_TMP
mkdir $LO_MOUNT
unionfs -o cow $LO_TMP=RW:$LO_SOURCE=RO $LO_MOUNT
libreoffice24.8 -env:UserInstallation=file://$LO_MOUNT --cat "$2" 2>/dev/null | grep --label="$2" -H -E -i -- "$1"
umount $LO_MOUNT
rmdir $LO_MOUNT
rm -rf $LO_TMP
