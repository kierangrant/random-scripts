#!/bin/bash
read -p "Enter Provider: " provider
read -p "Enter Account: " account
stty_orig=`stty -g`
stty -echo
read -p "Enter MFA Secret: " mfa_secret
stty $stty_orig
echo
echo -ne "otpauth://totp/${provider}:${account}?secret=${mfa_secret}&issuer=${provider}" | qrencode -t ANSIUTF8i
echo
