#!/usr/bin/sbcl --script

(defconstant +gits+
  (list
   "http://git.kpe.io/cl-base64"
   "http://git.kpe.io/clsql"
   "https://github.com/pcostanza/closer-mop"
   "http://git.kpe.io/puri"
   "https://github.com/archimag/restas"
   "https://github.com/cl-babel/babel"
   "https://github.com/metawilm/cl-python"
   "https://github.com/sharplispers/clx"
   "https://repo.or.cz/cl-elf"
   "https://github.com/sharplispers/cxml"
   "https://repo.or.cz/closure-common"
   "http://marijnhaverbeke.nl/git/parse-js"
   "https://github.com/VitoVan/xml-emitter"
   "https://github.com/antifuchs/idna"
   "https://github.com/archimag/cl-routes"
   "https://github.com/bluelisp/zip"
   "https://github.com/cffi/cffi"
   "https://github.com/cl-plus-ssl/cl-plus-ssl"
   "https://github.com/edicl/chunga"
   "https://github.com/edicl/cl-fad"
   "https://github.com/edicl/cl-ppcre"
   "https://github.com/edicl/cl-unicode"
   "https://github.com/edicl/cl-webdav"
   "https://github.com/edicl/cl-who"
   "https://github.com/edicl/drakma"
   "https://github.com/edicl/flexi-streams"
   "https://github.com/edicl/hunchentoot"
   "https://github.com/sharplispers/chipz"
   "https://github.com/sharplispers/ironclad"
   "https://github.com/sharplispers/nibbles"
   "https://github.com/gwkkwg/trivial-backtrace"
   "https://github.com/hankhero/cl-json"
   "https://github.com/jdz/rfc2388"
   "https://github.com/ljosa/cl-png"
   "https://github.com/madnificent/cl-recaptcha"
   "https://github.com/melisgl/named-readtables"
   "https://github.com/mishoo/cl-daemonize"
   "https://github.com/mishoo/cl-uglify-js"
   "https://github.com/nallen05/rw-ut"
   "https://github.com/pmai/md5"
   "https://github.com/sharplispers/parse-number"
   "https://github.com/sharplispers/split-sequence"
   "https://github.com/sionescu/bordeaux-threads"
   "https://github.com/sionescu/iolib"
   "https://github.com/sionescu/libfixposix"
   "https://github.com/sionescu/swap-bytes"
   "https://github.com/slime/slime"
   "https://github.com/tlikonen/cl-decimals"
   "https://github.com/tokenrove/anaphora"
   "https://github.com/trivial-features/trivial-features"
   "https://github.com/trivial-garbage/trivial-garbage"
   "https://github.com/trivial-gray-streams/trivial-gray-streams"
   "https://github.com/usocket/usocket"
   "https://github.com/vsedach/Parenscript"
   "https://github.com/vsedach/Vacietis"
   "https://github.com/vy/swank-daemon"
   "https://github.com/xach/salza2"
   "https://gitlab.common-lisp.net/alexandria/alexandria"
   "https://gitlab.common-lisp.net/asdf/asdf"
   "https://gitlab.common-lisp.net/cl-smtp/cl-smtp"
   "https://gitlab.common-lisp.net/clfswm/clfswm"
   "https://gitlab.common-lisp.net/cl-utilities/cl-utilities"
   "https://github.com/hargettp/cl-base32"
   "https://github.com/marijnh/ieee-floats"
   "https://github.com/marijnh/ST-JSON"
   "https://github.com/marijnh/parse-js"
   "https://github.com/edicl/cl-interpol"
   "https://github.com/AccelerationNet/cl-csv"
   "https://gitlab.common-lisp.net/iterate/iterate"
   "https://github.com/WarrenWilkinson/read-csv"))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (require "uiop"))

(loop for git in +gits+ do
     (if
      (not (uiop:directory-exists-p
	    (make-pathname
	     :directory
	     `(:relative
	       ,(subseq git
			(1+ (search "/" git :from-end t))
			(search ".git" git :from-end t))))))
      (progn
	(format t "Fetching: ~A~%" git)
	(uiop:run-program
	 `("git" "clone" ,git)
	 :input :interactive
	 :output :interactive
	 :error-output t))))
